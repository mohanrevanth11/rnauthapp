import React, {Component} from 'react';
import FireBase from 'firebase';
import {Text, StyleSheet} from 'react-native';
import {Button,Card,CardSection,Input,Spinner} from './common';

export default class LoginForm extends Component
{
    state = {email: '', password: '', error: '', loading: false};

    onButtonPress()
    {
        const {email, password} = this.state;
        this.setState({error: '', loading: true});
        FireBase.auth().signInWithEmailAndPassword(email, password)
        .then(this.onLoginSuccess.bind(this))
        .catch(() => {
            FireBase.auth().createUserWithEmailAndPassword(email,password)
            .then(this.onLoginSuccess.bind(this))
            .catch(this.onLoginFail.bind(this))
        });
    }

    onLoginFail(){
        this.setState({error: 'Authentication Failed', loading: false})
    }

    onLoginSuccess(){
        this.setState({
            email:'',
            password:'',
            loading: false,
            error: ''
        });
    }

    renderButton(){
        if(this.state.loading){
            return <Spinner size='small'/>
        }

        return <Button onPress={this.onButtonPress.bind(this)}>
                        Log In
        </Button> 
    }

    render(){
        return(
            <Card>
                <CardSection>
                    <Input
                        placeHolder = 'user@domain.com'
                        label = 'Email'
                        value = {this.state.email}
                        onChangeText={email => this.setState({email : email})}/>
                </CardSection>
                    
                <CardSection>
                    <Input
                        placeHolder = 'xxxx'
                        label = 'Password'
                        secureTextEntry
                        value = {this.state.password}
                        onChangeText={password => this.setState({password})}/>
                </CardSection>

                <Text style = {styles.errorText}>
                    {this.state.error}
                </Text>

                <CardSection>
                      {this.renderButton()}
                </CardSection>
            </Card>
        );
    }    
}

const styles = StyleSheet.create({
   errorText: {
       fontSize: 16,
       color: 'red',
       alignSelf: 'center'
   }
});