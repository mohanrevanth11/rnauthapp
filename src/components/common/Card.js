import React, {Component} from 'react';
import ReactNative, {Text, View, StyleSheet, Image} from 'react-native';

const Card = (props) => {
    return(
        <View style = {styles.container}>
            {props.children}
        </View>
    );
};

export {Card};

const styles = StyleSheet.create({

    container:{
        //borderWidth:1,
        //borderRadius: 2,
        //borderColor: '#DDD',
        //borderBottomWidth: 0,
        //shadowColor: '#000',
        //shadowOffset: {width:0, height: 2},
        //shadowOpacity: 0.1,
        elevation: 4,
        backgroundColor:'#FFF',
        position: 'relative',
        marginLeft: 8,
        marginRight: 8,
        marginTop: 16 

    }
}
);