//Import library
import {AppRegistry, View,Text,StyleSheet} from 'react-native';
import React, {Component} from 'react';

//Make component
const Header = (props) => {
    return (
        <View style = {styles.header}>
            <Text 
                style = {styles.headerText}>
                 {props.headerName}
            </Text>
        </View>
    );
}

//Make component available
export {Header};

//stylesheet

const styles = StyleSheet.create({

    header: {
        height:60,        
        paddingLeft:16,
        justifyContent: 'center',        
        backgroundColor: '#AF0000',
        elevation:16,
        position:'relative'
    },
    headerText: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#FFFFFF',
    }
});