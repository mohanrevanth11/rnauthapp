import React,{Component} from 'react';
import {TextInput, Text, View, StyleSheet} from 'react-native';

const Input = (props) => {

    return(
        <View style = {styles.containerStyle}>
            <Text style = {styles.labelStyle}> {props.label}</Text>
            <TextInput 
                placeholder = {props.placeHolder}
                style = {styles.inputStyle}
                value = {props.value}
                onChangeText = {props.onChangeText}
                secureTextEntry = {props.secureTextEntry}                                
            />
        </View>
    );
};

export {Input};

const styles = StyleSheet.create(
    {
        inputStyle: {
            color: '#000',
            paddingRight: 8,
            paddingLeft: 8,
            fontSize: 18,
            lineHeight: 23,
            flex: 2
        },
        labelStyle: {
            fontSize: 18,
            paddingLeft: 8,
            flex : 1
        },
        containerStyle: {
            height: 40,
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center'
        }
    }
);