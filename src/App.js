import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {AlbumHeader} from './components/common';
import LoginForm from './components/LoginForm'

export default class App extends Component{
    componentWillMount(){
        const Firebase = require("firebase");
        Firebase.initializeApp(
            {
                apiKey: 'AIzaSyDFAmrnAxBGGJKiEaI-5E2azLyp1wzC-m4',
                authDomain: 'authapprn.firebaseapp.com',
                databaseURL: 'https://authapprn.firebaseio.com',
                projectId: 'authapprn',
                storageBucket: 'authapprn.appspot.com',
                messagingSenderId: '796736601743'
            }
        );
    }
    render(){
        return(
            <View>
                <AlbumHeader headerName="Auth App"/>
                <LoginForm/>                    
            </View>
        );
    }
}